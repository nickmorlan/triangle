# system libs used
import argparse


# set up param parser
parser = argparse.ArgumentParser(description='''Enter three numbers representing the lenght of the sides of a triangle.''')
parser.add_argument('side1', help='First side')
parser.add_argument('side2', help='Second side')
parser.add_argument('side3', help='Third side')

args = parser.parse_args()

# validation
if len(args.type) != 3:
    exit(':You need to enter three numbers, representing the lenght of the sides of a triangle.')
if not args.side1.isDigit() or not args.side2.isDigit() or not args.side3.isDigit():
    exit(':You need to enter three numbers, representing the lenght of the sides of a triangle.')

sides        = [args.side1, args.side2, args.side3]
unique       = Set(sides)
unique_sides = len(unique)

if unique_sides == 1:
    print "This is an equalateral triangle."
elif unique_sides == 2:
    print "This is an isosceles trianle."
elif unique_sides == 3:
    print "This is an scalene trianle."
else:
    print "Something wierd has happened. Doublecheck your values and try again."