angular.module('triangleApp',[])


.controller('triangleCtl', ['$scope', 'Triangle',
                         function($scope, Triangle) {

    $scope.triangle = Triangle.init()
    $scope.$watch('triangle', function() {
      $scope.triangle.buildAndTest()
    }, true )

}])

.service('Triangle', function() {
    var side1, side2, side3 = 0
    var type = ''
    
    var init = function () {
      this.side1 = 0
      this.side2 = 0
      this.side3 = 0
      this.type  = 'not complete'
      
      return this
    }
    
    var buildAndTest = function() {
      if(this.side1 > 0 && this.side2 > 0 && this.side3 > 0) {
        test = [parseInt(this.side1), parseInt(this.side2), parseInt(this.side3)]
        sideLengths = uniqueify(test)
        switch (sideLengths.length) {
          case 1: 
            this.type = 'equalateral'
            break;
          case 2:
            this.type = 'isosoleces'
            break;
          case 3:
            this.type = 'scalene'
            break;
          default:
            this.type = 'not complete'
        }
      } else {
           this.type = 'not complete'
      }
    }
    
    var uniqueify = function (arr) {
      return arr.filter(function(value, index, self) { return self.indexOf(value) === index })  
    }
    
    return {
        init: init,
        buildAndTest: buildAndTest
    }
})