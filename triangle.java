package nick;

public class Triangle {
    private int side1, side2, side3;
    private string triangleType;

    public Triangle(int side1, int side2, int side3) {
        this.side1 = side1;
        this.side1 = side2;
        this.side1 = side3;
        this.determineType();
    }

    public string getType() {
        return this.triangleType;
    }

    private determineType() {
        if (this.side1 == this.side2 && this.side1 == this.side3) {
            this.triangleType = 'Equalateral';
        } elseif (this.side1 != this.side2 && this.side1 != this.side3 && this.side2 != this.side3) {
            this.triangleType = 'Scalene';
        } else {
            this.triangleType = 'Isosceles';
        }
    }
}


public class TraingleTest {
    public static void main(String[] args) {
        try {
            System.out.print("Enter the integer length of the side of a triangle to determine its type.");
            side1 = Integer.parseInt(System.console("Length of first side: "));
            side2 = Integer.parseInt(System.console("Length of second side: "));
            side3 = Integer.parseInt(System.console("Length of third side: "));

            Triangle test = new Triangle(side1, side2, side3);
            System.out.print(test.getType());

        } catch (Exception e) {
            System.out.print("Something went wrong. Call I.T. -- ");
            ex.printStackTrace();
        }

    }
}